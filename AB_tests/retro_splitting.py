import pandas as pd
import numpy as np
from scipy import spatial

def split_cosine(A, B, feature_weights=None, threshold=2):
    assert all(pd.api.types.is_numeric_dtype(A[col]) for col in A.columns), 'Таблицы должны содержать только числовой формат данных. Преобразуйте или избавьтесь от категориальных признаков'
    assert all(pd.api.types.is_numeric_dtype(B[col]) for col in B.columns), 'Таблицы должны содержать только числовой формат данных. Преобразуйте или избавьтесь от категориальных признаков'
    assert len(A) != 0 and len(B) != 0, 'Таблицы должны быть непусты'
    assert A.shape[1] == B.shape[1], 'Признаки у клиентов разных групп должны быть одинаковыми!'
    if feature_weights == None:
        feature_weights = np.ones(A.shape[1])   
    else:
         assert A.shape[1] == len(feature_weights), 'Проверьте признаки в векторе весов'
    distances_A = []
    distances_B = []

    A_ = A.to_numpy()
    B_ = B.to_numpy()

    A_ = A_ * feature_weights
    B_ = B_ * feature_weights

    A_mean_vector = np.mean(A_, axis=0)
    
    for i, object_ in enumerate(A_):
        if np.count_nonzero(object_) == 0:
            continue
        distance = spatial.distance.cosine(object_ - np.mean(A_, axis=0)/2, A_mean_vector - np.mean(A_, axis=0)/2)
        if distance <= threshold:
            distances_A.append([i, distance])
            
    for i, object_ in enumerate(B_):
        if np.count_nonzero(object_) == 0:
            continue
        distance = spatial.distance.cosine(object_ - np.mean(B_, axis=0), A_mean_vector - np.mean(B_, axis=0))
        if distance <= threshold:
            distances_B.append([i, distance])
            
    distances_A.sort(key=lambda i: i[1])
    distances_B.sort(key=lambda i: i[1])
    assert distances_A != [] and distances_B != [], 'При заданном Вами пороге нельзя подобрать похожие векторы в данных датасетах'
    if len(distances_A) <= len(distances_B):
        distances_B = np.array(distances_B)[:len(distances_A)].T[0]
        distances_A = np.array(distances_A).T[0]
    else:
        distances_A = np.array(distances_A)[:len(distances_B)].T[0]
        distances_B = np.array(distances_B).T[0]
    A = A.loc[A.index.isin(distances_A)]
    B = B.loc[B.index.isin(distances_B)]
    return A, B


