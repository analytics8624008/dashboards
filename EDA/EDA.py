
from ..dashboards.reporter import Reporter
from ..dashboards.visualizer import Visualizer
from ..dashboards.signalizer import Signalizer
from ..dashboards.conclusionizer import Conclusion

class EDA(Reporter, Visualizer, Signalizer, Conclusion):

    def __init__(self, test_name='EDA'):
        '''
            Как содержит функции полного цикла
            части анализа данных. В функции включены
            части проставления сигнала, словесного объяснения

            все функции работают на основе Pandas. И дополнительных
            уникальных аргументов. Предусматривает вложенности как 
            в Reporter.
        '''
        self.test_name = test_name
        return
    
    def check_target(self, df, target_name, target_type, date_name=None, date_group='month', submodel='overall', title='Анализ целевой переменной', sub_title=''):
        '''
            Функция проводит полный анализ целевой переменной.
            Позволяет получить анализ и рекомендации по целевой переменной, которых будет
            достаточно для более верного анализа и правильного выбора модели

            Включает в себя:
            1) Выведение основных статистик по целевой переменной
            2) Анализ распределения целевой переменной

            # Если переданно время
            3) Анализ тренда целевой переменной
            4) Анализ стабильности целевой переменной во времени

        '''
        if date_name != None:
            if target_type == 'categorial':
                number_miss = df['target']
                self.report.setdefault(submodel, {})
            unuqie_values = target_name
        return