from ..dashboards import reporter, conclusionizer, signalizer, visualizer

from utils.product_metrics import compute_churn_rate, compute_clv, compute_retention



class ProductEDA(reporter, visualizer, signalizer, conclusionizer):

    def __init__(self, report_name='EDA_Product'):
        self.report_name = report_name
    
    def plese_report_retention(self, df, date, client_id, model, days=24, title='', sub_title=''):

        table = compute_retention(df=df, date=date, client_id=client_id, days=days)
        graph = self.please_visualize_table(df=df)
        signal = None
        conclusion = None
        content = {
            'sub_title': {
            'table': table,
            'graph': graph,
            'type': 'graph',
            'signal': signal,
            'conclusion': conclusion
            }
        }
        self.add_test(
            model=model,
            test_name=title,
            sub_titles=content
        )
        return content
