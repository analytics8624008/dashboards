import pandas as pd
import numpy as np
import datetime as dt

def compute_clv(df, date_name, client_id, sum_name, rk_name, trafic_name=None,):
    '''
        Нужные столбцы
            - Дата
            - id источника трафика (Если есть)
            - id клиента
            - Сумма покупки клиентом
            - Кол-во покупок клиентом
            - Траты на продвижение
    '''
    average_order_value = df[sum_name].sum() / len(df)
    unique_clients = df[client_id].unique()
    purchase_frequency = len(df) / unique_clients
    customer_value = average_order_value * purchase_frequency

    client_time = []
    for client in unique_clients:
        df_client = df[df[client_id] == client]
        max_date = df_client[date_name].max()
        min_date = df_client[date_name].min()
        diff_day = max_date - min_date
        client_time.append(diff_day)

def compute_churn_rate(df, date, client_id, sum, window=2):
    df = df.groupby([date, client_id])[sum].count().reset_index()
    df.columns = [date, client_id, 'count']
    df['next_event_date'] = df.groupby(client_id)[date].shift(-1)
    df['days_till_next_event'] = (df['next_event_date'] - df[date]) / np.timedelta64(1, 'D')
    df['is_churned_int'] = df['days_till_next_event'].apply(lambda x: 0 if x < window else 1)

    max_relevant_date = df[date].max() - dt.timedelta(days=window)
    churn_rate = df.query(f'{date} <= @max_relevant_date')\
                            .groupby([date])\
                            .agg({'is_churned_int' : 'sum', client_id : 'nunique'})
    churn_rate.columns = ['churned', 'dau']
    # расчитываем метрику
    churn_rate['churn_rate'] = churn_rate['churned'] / churn_rate['dau']
    # переводим ее в проценты
    churn_rate['churn_rate_perc'] = churn_rate['churn_rate'] * 100

    return churn_rate


def compute_retention(df, date, client_id, days=20):
    start_date = df.groupby(client_id)[date].min().rename('start_date')
    df = pd.merge(df, start_date, left_on=client_id, right_index=True)
    df['day'] = (df[date] - df['start_date']).dt.days
    
    # Создаем список для хранения classic retention на каждый день
    table = []
    days = list(range(0, days))
    # Рассчитываем classic retention для каждого дня
    for day in days:        
        # Выбираем пользователей, вернувшихся в день `day`
        users_with_classic_day = df[(df['day'] == day)][client_id].unique()
        users_with_rolling_day = df[df['day'] >= day][client_id].unique()

        # Получаем уникальные дни активности для каждого пользователя
        unique_days = df.groupby(client_id)['day'].unique()
        expected_days = set(range(1, day + 1))
        
        # Определяем пользователей с полным удержанием до retention_day
        full_retention_users = unique_days[unique_days.apply(lambda x: set(x) > expected_days)].index
        
        # Рассчитываем rolling retention для данного дня
        table.append(
            {
                'rolling_retention': len(users_with_rolling_day) / len(df[client_id].unique()),
                'classic_retention': len(users_with_classic_day) / len(df[client_id].unique()),
                'full_retention': len(full_retention_users) / len(df[client_id].unique()),
            }
        )
    table = pd.DataFrame(table)
    table.columns = [str(col) + '_line' for col in table.columns]
    return table