
from reporter import Reporter
'''
    Итак, этот тест помогает проверят этапы выполнения
    анализа в зависимости от типа анализа. 

    Делается это на основе отчета, соххраненного посредством
    класса Report. Класс призван оценить качество проведенного
    анализа а именно:
    1) Его полноту
    2) Отсуствие дублей тестов


'''
class AnalyticsSystem():

    type_test_realize = []

    def __init__(self, type_analysis, report):
        self.type_analysis = type_analysis
        self.report = report
        self.stage_of_analysis = [
            'EDA',
            'PREPROCESSING',
            'FITTING',
            'AGGREGATE RESULTS',
            'CONCLUSION RESULTS',
            'STABILITY ANALYSIS',
            'BULID SYSTEM',
            'MONITORING',
            'RE-ANALYSIS'
        ]

    def check_analysis(self):
        return




