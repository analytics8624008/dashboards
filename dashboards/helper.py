import pandas as pd
import numpy as np


class Helper():

    def processor_df_date(self, df, date:str):
        is_date = (df[date].select_dtypes(include=[np.datetime64]))
        return df