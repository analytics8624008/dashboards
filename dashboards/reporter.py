import pandas as pd
import plotly
from typing import Union
import plotly.graph_objects as go


class Reporter():
    '''
        Нужен для визуализации и сохранении результатов тестирования 
        и разработки. Также для визуализации дашбордов и вообще
        представления аналитики в удобочитаемом виде

        Работает посредством сохранения данных в словарь со следующей структурой

        'Название теста': {
            'Модель или подмодель': - также любая абстракция для раздления и сравнения результатов при необходимости
                'Имя теста (Загоовок)': {
                    'Подтип теста (Подзаголовок)': - для дополнительно разделения результатов тестирования. 
                                    допустим этот тест для каждой переменной. Или этот тест с разным
                                    временным интервалом.
                        {
                            'type': 'table',
                            'table': pd.DataFrame,
                            'graph': {
                                'type': in [' plotly', 'chartjs']: (graph object)
                            },
                            'card_value': 28,
                            'conclusion': 'Описание результатов тестирования - их интепритация'
                            'signal': in ['green', 'yellow', 'red', 'grey'],
                            'score': 0 ... 10, - числовое отображение сигнала - качеста теста
                        }
                }
        }

        'settings': {
            'tests': {
                'test_name': {'in_word': True, 'in_dash': true, 'in_signal': True, signal_weight: 0..1.0; 'type_agg_signal': 'mean_score'}
            }
            'overall': {
                
            }
        }
    '''
    keys_conclusion = {
        'type': '(Обязательно) Тип контента, который нужно визуализировать. in ["table", "graph", "card"]',
        'table': '(Необязательно) (Обязателен, если тип "table") Передается, если тип table. Таблица pd.DataFrame.',
        'card': '(Необязательно) (Обязателен, если тип "card") Числовое значение карточки. Также может быть текст',
        'graph': '(Необязательно) (Обязателен, если тип "graph") Передается, если тип контента "graph". Принимать словарь. {"plotly": (plotly_object), "chartjs": (dict_chart_js)}. При работе с отчетами ворд и dash. Можно передать словарь с одник ключем -  plotly',
        'conclusion': '(Необязательно) Описание резултатов тестов. Поменятка или анализ связей, который важно отобрить под графиком.',
        'signal': '(Необязательно) Оценка качества теста. Нужна для агрегации общего сигнала - положения дел. in ["green", "yellow", "red", "grey"]',
        'score': '(Необязательно) Обозначение качества сигнала в числовом эквиваленте. от 0 до 10'
    }
    required_types = {
        'table': [pd.DataFrame],
        'graph': [plotly, go, go.Figure()],
        'card': [str, float, int]
    }
    def __init__(self, test_name='Результаты анализа'):
        self.report = {}
        self.key_in_sub_titles = ['type', 'table', 'graph', 'conclusion', 'signal', 'score', 'size']
        self.required_keys = ['table']

    def add_test(self, model:str, test_name:str, sub_titles:dict=None, replace=False, settings_test:dict=None):
        '''
            model: название модели для сравнения или анализа, также может выступать любым
                разделением тестов. Допустим разные продуктовые точки, разние системы аналитики
            test_name: уникальное название теста в разрезе подмодели
            content: dict: словарь соотвествующий типу выше
            sub_titles: словарь с ключами. Передается если есть необходимость передать уже заполнный подзаголовками тест
        '''
        # Создаем ключи, если таких нет.
        self.report.setdefault(model, {})
        self.report[model].setdefault(test_name, {})
        if replace == True:
            self.report[model][test_name] = {}

        # Назначаю настройки для заголовка

        # Проверка, что значения подзаголовков уникальные
        self.check_unuqie_subtitles(sub_titles)
        # И теперь добавляем
        for sub_title, sub_title_content in sub_titles.items():
            # Проверка адекватности переданных подзаголовков
            check_status = self.check_subtitles(sub_title_name=sub_title, sub_title_content=sub_title_content)
            if check_status == True:
                self.report[model][test_name][sub_title] = sub_title_content
        

    def check_subtitles(self, sub_title_name, sub_title_content):
        erros = 0
        keys_not_in = [key for key in sub_title_content.keys() if key not in self.key_in_sub_titles]
        required_not_in = [key for key in keys_not_in if key in self.required_keys]
        # Проверка, что присутсвует обязательное поле
        if len(required_not_in) != 0:
            print(f'Вы не указали необходимые ключи для  sub_title {sub_title_name}. Добавление в отчет невозможно, пропущен.')
            print('Отсутсвуют следующие необходимые ключи: ', str(required_not_in))
            return False
        # Сообщение, что остуствуют некоторые поля, которые могут быть полезны
        if len(keys_not_in) != 0:
            erros += 1
            print('Не указаны следующие ключи: ')
            for key in keys_not_in:
                key_conclusion = self.keys_conclusion[key]
                print(f'{key}: {key_conclusion}')
        # Проверить тип переданных данных для визуализации
        '''
        content_type = sub_title_content['type']
        content_in = sub_title_content[content_type]
        type_content_in = type(content_in)
        required_type = self.required_types[content_type]   
        if not isinstance(content_in, type(required_type)):
            print(f'Вы передали тип {content_type}, который трубует, следующий тип данных {required_type}. Передан - {type_content_in}')
            return False'''
        
        if erros == 0:
            print(f'{sub_title_content}: Заполненно идеально, вы используйте все возможности! Контент добавлен в отчёт.')
        
        else:
            print(f'{sub_title_content}: Контент добавлен в отчёт, но вы не используете все возможности.')
        return True

    def check_unuqie_subtitles(self, sub_titles):
        not_unique = []
        for key in sub_titles.keys():
            count = list(sub_titles.keys()).count(key)
            if count > 1:
                not_unique.append(key)
        if len(not_unique) != 0:
            print('Вы передали неуникальные заголовки, в отчет добавятся, только последний из них. ')
            print('Неуникальные ключи: ', str(not_unique))


    def set_settings(self):
        return

