import plotly.graph_objects as go

class Visualizer():


    def please_visualize_table(self, df, stacked_bar=False, title='График', x_name='x', y_name='y'):
        '''
            Принимает на вход df, в котором индекс -
            это ось X. колонки - значения (ось y).

            Колонки должны быть названы следующим образом
            (type plot)_Имя - все в названиях считается аргументом между 
            знаком "_". 

            type plot in ["bar", "line"]

            Если указано несколько столбцов с типом bar,  то
            их можно визуализировать в формате stacked.
            Для этого нужно передать аргументу stacked_bar = True.
        '''

        all_columns = df.columns
        bar_columns = []
        line_columns = []
        for col in all_columns:
            col_split = col.split('_')
            plot_type = col_split[0]
            if plot_type == 'line':
                line_columns.append(col)
            elif plot_type == 'bar':
                bar_columns.append(col)

        # Визуализирую plotly
        fig = go.Figure()
        for column_name in line_columns:
            fig.add_trace(go.Scatter(x=df.index, y=df[column_name], name=column_name))
        for column_name in bar_columns:
            fig.add_trace(go.Bar(x=df.index, y=df[column_name], name=column_name))

        fig.update_layout(legend_title_text = "Contestant")
        fig.update_xaxes(title_text="Fruit")
        fig.update_yaxes(title_text="Number Eaten")
        fig.show()
        return fig
        